package com.vreality.model.interview;

import com.vreality.model.actors.Role;
import com.vreality.model.actors.authentication.SocialProvider;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * Created by noam on 3/23/14.
 */
@Entity
@Table(name="INTERVIEWER")
@PrimaryKeyJoinColumn(name="PERSON_ID")
@DiscriminatorValue("I")
public class Interviewer extends Person {
    public Interviewer(String email, String fname, String lname, SocialProvider signInProvider, String providerUserId) {
        super(email, fname, lname, Role.INTERVIEWER, signInProvider, providerUserId);
    }

    public Interviewer(String email, String fname, String lname){
        super(email, fname, lname, Role.CANDIDATE);
    }

    public Interviewer() {
        super();
    }
}

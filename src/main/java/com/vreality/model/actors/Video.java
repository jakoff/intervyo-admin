package com.vreality.model.actors;

/**
 * Created by noam on 3/24/14.
 */
public class Video {
    protected Integer id; // the video ID
    protected String source; // the url
    protected Integer length; // in seconds
    protected String transcript; // The text said in this video
    protected Long size; // In bytes
    protected String format; // also called sourceType - values: h264, webm, ogg, vp9

    public Video(String source, Integer length) {
        this.source = source;
        this.length = length;
    }

    private Video(String source, Integer length, String transcript) {
        this(source, length);
        this.transcript = transcript;
    }

    private Video(String source, Integer length, String transcript, Long size) {
        this(source, length, transcript);
        this.size = size;
    }

    public Video(String source, Integer length, String transcript, Long size, String format) {
        this(source, length, transcript, size);
        this.format = format;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public Integer getLength() {
        return length;
    }

    public String getTranscript() {
        return transcript;
    }

    public Long getSize() {
        return size;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }
}

package com.vreality.model.actors;

/**
 * Created by noam - 8/14/14
 */
public enum Role {
    CANDIDATE,
    INTERVIEWER,
    ADMIN
}

package com.vreality.dao;

import com.vreality.model.actors.authentication.Account;
import com.vreality.model.actors.authentication.AccountUtil;
import com.vreality.model.interview.Person;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by noam - 8/13/14
 */
@Repository
public class PersonDaoImpl extends SpringHibernateDao implements PersonDao {

    @Override
    public void savePerson(Person person) {
        getSession().saveOrUpdate(person);
    }

    @Override
    public Person loadPerson(Class clazz, Long id) {
        return (Person) getSession().load(clazz, id);
    }

    @Override
    public Account findAccountByUsername(String username) {
        final Person person = findPersonByUsername(username);
        return AccountUtil.getAccountFromPerson(person);
    }

    @Override
    public Person findPersonByUsername(String username) {
        String hql = "from Person person where person.email = :username";
        Query query = getSession().createQuery(hql);
        query.setParameter("username", username);

//        ( (Integer) session.createQuery("select count(*) from ....").iterate().next() ).intValue()
        return (Person) query.uniqueResult();
    }

}


package com.vreality.service.user;

/**
 * Created by noam - 8/17/14
 */
public class UsernameAlreadyInUseException extends Exception {
    public UsernameAlreadyInUseException(String username) {
        super("The username '" + username + "' is already in use.");
    }
}
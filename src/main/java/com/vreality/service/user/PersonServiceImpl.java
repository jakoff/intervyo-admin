package com.vreality.service.user;

import com.vreality.dao.PersonDao;
import com.vreality.model.actors.authentication.Account;
import com.vreality.model.interview.Candidate;
import com.vreality.model.interview.Person;
import org.hibernate.HibernateException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.orm.hibernate4.SessionFactoryUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by noam - 8/14/14
 */
@Service
@Transactional
public class PersonServiceImpl implements PersonService {

    @Autowired
    PersonDao personDao;

    @Override
//    @Transactional(readOnly = true, propagation= Propagation.SUPPORTS)
    public void savePerson(Person person) throws UsernameAlreadyInUseException{
        try{
            personDao.savePerson(person);
        } catch (HibernateException e) {
            DataAccessException exception = SessionFactoryUtils.convertHibernateAccessException(e);
            if (exception instanceof DuplicateKeyException || e instanceof ConstraintViolationException){
                throw new UsernameAlreadyInUseException(person.getEmail());
            }
        }
    }

    @Override
    public Person getPerson(Class clazz, Long personId) {
        return personDao.loadPerson(clazz, personId);
    }

    @Override
    public void createAccount(Account account) throws UsernameAlreadyInUseException {
//      TODO: Differentiate candidate from Interviewer before saving
        Candidate candidate = new Candidate(account.getUsername(), account.getFirstName(), account.getLastName());
        savePerson(candidate);
    }

    @Override
    public Person findPersonByUsername(String username) {
        return personDao.findPersonByUsername(username);
    }

    @Override
    public Account findAccountByUsername(String username) {
        return personDao.findAccountByUsername(username);
    }
}

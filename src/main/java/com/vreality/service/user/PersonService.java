package com.vreality.service.user;

import com.vreality.model.interview.Person;

import java.sql.SQLException;

/**
 * Created by noam - 8/14/14
 */
public interface PersonService extends AccountRepository {
    public void savePerson(Person person) throws UsernameAlreadyInUseException, SQLException;
    public Person getPerson(Class clazz, Long personId);
}

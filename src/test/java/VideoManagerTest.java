import com.vreality.service.video.VideoManager;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

/**
 * Created by noam - 5/19/14
 * Validates the VideoManager logic
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/spring-test-config.xml"})
public class VideoManagerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    VideoManager videoManager;

    @Test
    public void thereAreSomeVideos(){
       assertTrue(videoManager.getVideos().size() > 0);
    }
}

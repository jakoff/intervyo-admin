#!/bin/bash

gradle war
scp -i ../vreality.pem ../build/libs/intervyo.war ubuntu@54.76.51.74:/tmp

ssh -i vreality.pem ubuntu@54.76.51.74 <<'ENDSSH'
#commands to run on remote host
sudo cp /tmp/intervyo.war /var/lib/tomcat7/webapps/interview.war
#sudo service tomcat7 restart
ENDSSH